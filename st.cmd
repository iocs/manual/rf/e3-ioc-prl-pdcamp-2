# PRL
# 2 Connections one for SPI and one for I2C

require prlamppdc
require essioc

# Prefix for all records
epicsEnvSet("P",           "MBL-070PRL:")
epicsEnvSet("R",           "RFS-PRLPDC-002:")
epicsEnvSet("R_AMP",           "RFS-PRLAmp-002:")
epicsEnvSet("DEVICE_IP",        "172.16.100.117")

# For amplifiers
epicsEnvSet("SPI_COMM_PORT",    "AK_SPI_COMM")
# For PDC
epicsEnvSet("I2C_COMM_PORT",    "AK_I2C_COMM")

# Load standard module startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

#- Create the asyn port to talk XTpico server on TCP port 
drvAsynIPPortConfigure($(SPI_COMM_PORT),"$(DEVICE_IP):1002")
drvAsynIPPortConfigure($(I2C_COMM_PORT),"$(DEVICE_IP):1003")

#asynSetTraceIOMask("$(I2C_COMM_PORT)",0,255)
#asynSetTraceMask("$(I2C_COMM_PORT)",0,255)

PRLAmpPDCConfigure("PRL", "$(SPI_COMM_PORT)", "$(I2C_COMM_PORT)", 0x32, 0, 0)
dbLoadRecords("PRL_Amp_PDC_MO2.db", "P=$(P),R=$(R), R_AMP=$(R_AMP),PORT=PRL,ADDR=0,TIMEOUT=1")

iocshLoad("$(E3_CMD_TOP)/iocsh/PDC.iocsh")
#asynSetTraceIOMask("PRLHPA.$(N)",0,255)
#asynSetTraceMask("PRLHPA.$(N)",0,255)
